enablePlugins(JavaAppPackaging, DockerPlugin)

organization := "kz.su"

name := "unsplash-api"

version := "0.1"

dockerBaseImage := "openjdk:8-jre"
packageName in Docker := "unsplash-api"
mainClass in Compile := Some("kz.su.unsplash.Boot")

scalaVersion := "2.12.8"

lazy val akkaHttpVersion = "10.1.1"
lazy val akkaVersion = "2.6.0-M3"
lazy val json4sVersion   = "3.5.4"
lazy val domainVersion = "0.1"
lazy val elastic4sVersion = "6.1.2"

libraryDependencies ++= Seq(
  "com.typesafe.akka"      %% "akka-http"                       % akkaHttpVersion,
  "com.typesafe.akka"      %% "akka-http-spray-json"            % akkaHttpVersion,
  "de.heikoseeberger"      %% "akka-http-json4s"                % "1.20.1",

  "com.typesafe.akka"      %% "akka-stream"                     % akkaVersion,
  "com.typesafe.akka"      %% "akka-slf4j"                      % akkaVersion,
  "com.typesafe.akka"      %% "akka-actor"                      % akkaVersion,
  "com.typesafe.akka"      %% "akka-testkit"                    % akkaVersion,
  "org.json4s"             %% "json4s-core"                     % json4sVersion,
  "org.json4s"             %% "json4s-jackson"                  % json4sVersion,
  "org.json4s"             %% "json4s-native"                   % json4sVersion,

  "kz.su"                  %% "unsplash-domain"                 % domainVersion,

  "com.sksamuel.elastic4s" %% "elastic4s-jackson"               % elastic4sVersion,
  "com.sksamuel.elastic4s" %% "elastic4s-core"                  % elastic4sVersion,
  "com.sksamuel.elastic4s" %% "elastic4s-http"                  % elastic4sVersion,
  "com.sksamuel.elastic4s" %% "elastic4s-json4s"                % elastic4sVersion,
  "com.sksamuel.elastic4s" %% "elastic4s-tcp"                   % elastic4sVersion,

  "org.scalatest"          %% "scalatest"                       % "3.0.5"   % "test"
)