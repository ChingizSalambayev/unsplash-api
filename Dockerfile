FROM openjdk:8-jre

MAINTAINER Chingiz Salambayev

ENV LANG=ru_RU.UTF-8
ENV APP_NAME unsplash-api-0.1.zip
ENV APP_DIR unsplash-api-0.1
ENV RUN_SCRIPT unsplash-api
ENV JAVA_OPTS -server -Xms128M -Xmx1024M -Xss1M -XX:+CMSClassUnloadingEnabled

WORKDIR /root
COPY ./target/universal/$APP_NAME /root/
RUN unzip -q $APP_NAME
WORKDIR /root/$APP_DIR/bin

RUN rm /root/$APP_NAME

RUN chmod +x $RUN_SCRIPT

CMD ./$RUN_SCRIPT