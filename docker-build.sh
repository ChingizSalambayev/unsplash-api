#!/bin/zsh

set -e

REMOTE_IP=chingizsalambayev
DOCKER_IMAGE_NAME=unsplash-api
VERSION=v0.1

case $1 in
    "dev")
        sbt clean compile dist
        docker build --no-cache -t $DOCKER_IMAGE_NAME .
        docker tag $DOCKER_IMAGE_NAME:latest $REMOTE_IP/$DOCKER_IMAGE_NAME:$VERSION
        docker push $REMOTE_IP/$DOCKER_IMAGE_NAME:$VERSION
    ;;
esac