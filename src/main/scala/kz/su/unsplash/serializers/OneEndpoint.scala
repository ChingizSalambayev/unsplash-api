package kz.su.unsplash.serializers

import kz.su.exceptions.{SuErrorSeries, SuErrorSystem}

trait OneEndpoint {
  val errorSystem = SuErrorSystem
  val errorSeries = SuErrorSeries.CORE
}