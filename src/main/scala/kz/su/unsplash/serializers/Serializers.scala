package kz.su.unsplash.serializers

import kz.su.serializer.CoreSerializers
import org.json4s.native.Serialization
import org.json4s._

trait Serializers extends CoreSerializers {

  implicit val formats = Serialization.formats(
    ShortTypeHints(
      coreTypeHints
    )
  )

  implicit val serialization = jackson.Serialization

}