package kz.su.unsplash.api

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.BasicDirectives
import akka.http.scaladsl.server.{Route, RouteResult}
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import kz.su.serializer.DomainObject
import kz.su.unsplash.actors.UnsplashActor.{GetListPhotos, GetPhotoById, GetPhotosFromCollection, GetRandomPhoto, GetRelatedCollections, SearchCollections, SearchPhotos}
import kz.su.unsplash.cors.CORSHandler
import kz.su.unsplash.serializers.Serializers

import scala.concurrent.Promise

trait RestRoutes extends Json4sSupport
  with BasicDirectives
  with PerRequestCreator
  with Serializers
  with CORSHandler {

  implicit def system: ActorSystem

  def unsplashActor: Props

  def unsplashRoutes: Route = {
    corsHandler(
      pathPrefix("photos") {
        concat(
        pathEndOrSingleSlash {
          parameters(
            'page.as[Int] ? 1,
            'per_page.as[Int] ? 10,
            'order_by.as[String] ? "latest"
          ) { (page, pageSize, orderBy) =>
            get {
              handleRequest(unsplashActor,
                GetListPhotos(
                  page = page,
                  pageSize = pageSize,
                  orderBy = orderBy
                )
              )
            }
          }
        } ~
          path("id" / Segment) { photoId =>
            get {
              handleRequest(unsplashActor,
                GetPhotoById(
                  id = photoId
                )
              )
            }
          } ~
          path("random") {
            get {
              handleRequest(unsplashActor, GetRandomPhoto())
            }
          }
        )
      } ~
        pathPrefix("search") {
          concat(
            path("photos") {
              get {
                parameters(
                  'query.as[String] ? "",
                  'page.as[Int] ? 1,
                  'pageSize.as[Int] ? 10,
                  'orientation.as[String] ? ""
                ) { (query, page, pageSize, orientation) =>
                  handleRequest(unsplashActor,
                    SearchPhotos(
                      query = query,
                      page = page,
                      pageSize = pageSize,
                      orientation = orientation
                    )
                  )
                }
              }
            } ~
            path("collections"){
              get {
                parameters(
                  'query.as[String] ? "",
                  'page.as[Int] ? 1,
                  'pageSize.as[Int] ? 10
                ) { (query, page, pageSize) =>
                  handleRequest(unsplashActor,
                    SearchCollections(
                      query = query,
                      page = page,
                      pageSize = pageSize
                    )
                  )
                }
              }
            } ~
            path("collection" / Segment / "photos") { collectionId =>
              get {
                handleRequest(unsplashActor,
                  GetPhotosFromCollection(
                    collectionId = collectionId
                  )
                )
              }
            } ~
            path("collection" / Segment / "related") { collectionId =>
              get {
                handleRequest(unsplashActor,
                  GetRelatedCollections(
                    collectionId = collectionId
                  )
                )
              }
            }
          )
        }
    )
  }

  def handleRequest(targetProps: Props, message: DomainObject): Route = ctx => {
    val p = Promise[RouteResult]
    perRequest(ctx, targetProps, message, p)(system)
    p.future
  }
}