package kz.su.unsplash.api

import java.util.Locale

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{OneForOneStrategy, _}
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.headers.`Accept-Language`
import akka.http.scaladsl.model.{StatusCode => AkkaStatusCode, StatusCodes => AkkaStatusCodes}
import akka.http.scaladsl.server.{RequestContext, RouteResult}
import com.typesafe.config.ConfigFactory
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import kz.su.unsplash.api.PerRequest.{WithActorRef, WithProps}
import kz.su.exceptions.{ApiException, ErrorInfo, GatewayTimeoutErrorException, ServerErrorRequestException, SuErrorCodes}
import kz.su.serializer.{Accepted, DomainEntity, DomainObject, SeqEntity}
import kz.su.unsplash.serializers.{OneEndpoint, Serializers}
import org.json4s.JsonAST.JValue

import scala.concurrent.Promise
import scala.concurrent.duration._



case class CustomResponse(status: AkkaStatusCode, entity: Option[ToResponseMarshallable])

trait PerRequest extends Actor with ActorLogging with Serializers with OneEndpoint with Json4sSupport {

  import context._

  def r: RequestContext
  def target: ActorRef
  def message: DomainObject
  def p: Promise[RouteResult]
  def stan = System.nanoTime()

  val config = ConfigFactory.load()

  setReceiveTimeout(Duration(config.getString("akka.http.server.request-timeout")))
  target ! message

  def receive: Receive = {
    case res: Accepted => complete(AkkaStatusCode.int2StatusCode(res.status), res)
    case res: String => complete(AkkaStatusCodes.OK, res)
    case res: Seq[Any] => complete(AkkaStatusCodes.OK, res)
    case jvalue: JValue => complete(AkkaStatusCodes.OK, jvalue)
    case map: Map[_, _] => complete(AkkaStatusCodes.OK, map)
    case res: SeqEntity[_] => complete(AkkaStatusCodes.OK, res.entities)
    case res: Seq[DomainEntity] => complete(AkkaStatusCodes.OK, res)
    case res: DomainEntity => complete(AkkaStatusCodes.OK, res)
    case e: ApiException => completeWithError(e)
    case e: Exception => completeWithError(ServerErrorRequestException(SuErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem), Some(e.getMessage)))
    case ReceiveTimeout => completeWithError(GatewayTimeoutErrorException(SuErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem)))
    case e: ErrorInfo =>
      if (e.status.isDefined) {
        complete(AkkaStatusCode.int2StatusCode(e.status.get), e)
      } else {
        //        log.error(s"$stan Error Info does not contain status code: $e")
        completeWithError(ServerErrorRequestException(SuErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem)))
      }
  }

  def complete(status: AkkaStatusCode, obj: => ToResponseMarshallable) = {

    val f = status match {
      case AkkaStatusCodes.NoContent =>

        r.complete(None)

      case _ =>

        r.complete(obj)

    }

    f.onComplete(p.complete(_))

    stop(self)
  }

  /**
    * Completes request with error
    *
    * @param apiException
    */
  def completeWithError(apiException: ApiException) = {
    val language = RequestUtil.getLanguageFromRequestContext(r)
    val errorInfo = apiException.getErrorInfo(ErrorLocaleContextFactory.getContextForLocale(new Locale(language)))
    log.error(s"[$stan] Got exception: ${apiException.toString}")
    log.error(s"[$stan] Application return expected error status code [${language}] ${apiException.status} with entity ${errorInfo} ")
    complete(AkkaStatusCode.int2StatusCode(apiException.status.intValue), errorInfo)
    stop(self)
  }

  override val supervisorStrategy =
    OneForOneStrategy() {

      /**
        * Catching Api Exceptions
        */
      case e: ApiException => {

        completeWithError(e)

        Stop
      }

      /**
        * Catching any other exceptions
        */
      case e => {

        val error = ServerErrorRequestException(SuErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem), Some(e.getMessage))
        completeWithError(error)

        Stop

      }
    }
}

object PerRequest {

  case class WithActorRef(r: RequestContext, target: ActorRef, message: DomainObject, p: Promise[RouteResult]) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: DomainObject, p: Promise[RouteResult], actorName: Option[String] = None) extends PerRequest {
    lazy val target = actorName match {
      case Some(name) => context.actorOf(props, name)
      case _ => context.actorOf(props)
    }
  }

}

trait PerRequestCreator {

  def perRequest(r: RequestContext, target: ActorRef, message: DomainObject, p: Promise[RouteResult])(implicit system: ActorSystem) =
    system.actorOf(Props(new WithActorRef(r, target, message, p)))

  def perRequest(r: RequestContext, props: Props, message: DomainObject, p: Promise[RouteResult], actorName: Option[String] = None)(implicit system: ActorSystem) = actorName match {
    case Some(name) => system.actorOf(Props(new WithProps(r, props, message, p, actorName)), s"parent-$name")
    case _ => system.actorOf(Props(new WithProps(r, props, message, p, None)))
  }

}

object RequestUtil {

  val defaultLanguage = "in"

  /**
    * Returns requested language from header.
    * If not presented, then default language is returned
    *
    * @param context - request context
    * @return language as string
    */
  def getLanguageFromRequestContext(context: RequestContext): String = {

    context.request.headers.find(_.name.equalsIgnoreCase("Accept-Language")) match {

      case Some(header) =>

        val lan: Option[String] = header match {

          case `Accept-Language`(languages) =>

            languages.find(_.primaryTag != "null").map(_.primaryTag)

          case _ =>

            LocalizedMessages.locales.find(header.value.indexOf(_) >= 0)

        }

        lan match {
          case Some(l) => l
          case _ => defaultLanguage
        }

      case _ => defaultLanguage

    }

  }

  def getLanguageFromHeader(header: Map[String, String]): String = {

    header.find(_._1.equalsIgnoreCase("Accept-Language")) match {
      case Some(lh) =>

        LocalizedMessages.locales.find(lh._2.indexOf(_) >= 0) match {
          case Some(l) => l
          case _ => defaultLanguage
        }

      case _ => defaultLanguage
    }

  }

}
