package kz.su.unsplash.util

import java.security.cert.X509Certificate

import akka.actor.ActorSystem
import akka.http.scaladsl.{ConnectionContext, Http}
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import com.typesafe.sslconfig.akka.AkkaSSLConfig
import javax.net.ssl.{KeyManager, SSLContext, X509TrustManager}
import kz.su.unsplash.serializers.Serializers

import scala.concurrent.Future

trait RestClient {

  /**
    * Sends POST request with data to remote server
    *
    * @param url   - url to call
    * @param data  - data to send
    * @param query - query parameter to send
    * @param headers - http headers
    * @tparam T    - marshaller
    * @return http response
    */
  def post[T](url: String, data:T,  query: Option[Map[String, String]] = None, headers: Option[Map[String, String]] = None): Future[HttpResponse]

  val customContentTypeUrlencoded = ContentType(MediaType.customWithOpenCharset("application", "x-www-form-urlencoded"), HttpCharsets.`UTF-8`)

  val customContentTypeSoap = ContentType(MediaType.customWithOpenCharset("application", "soap+xml"), HttpCharsets.`UTF-8`)

}
//
/**
  * Rest client implementation
  *
  * @param endpoint - service endpoint
  * @param accept   - content type to accept
  * @param system   - actor system
  */
class RestClientImpl(endpoint: String, accept: String = "application/json")(implicit val system: ActorSystem) extends RestClient with Serializers {

  /**
    * Builds url with query param
    *
    * @param url   - url to call
    * @param query - query parameters to append
    * @return Uri
    */
  def buildUri(url: String, query: Option[Map[String, String]] = None, parameters: Option[String] = None): Uri = {

    val fullUrl = endpoint + (if (endpoint.endsWith("/")) url else "/" + url)

    val uri: Uri = query match {
      case Some(map) if map.nonEmpty =>
        Uri(fullUrl.toString).copy(rawQueryString = Some(Query(map).toString))
      case _ =>
        Uri(fullUrl.toString)
    }

    uri

  }

  def buildGetUri(url: String, parameters: Option[String] = None): Uri = {

    val fullUrl = endpoint + (if (endpoint.endsWith("/")) url else "/" + url)

    val uri: Uri = parameters match {
      case Some(param) if param.nonEmpty =>
        Uri(fullUrl.toString).copy(path = Path(param))
      case _ =>
        Uri(fullUrl.toString)
    }

    uri

  }

  def prepareHeaders(headers: Option[Map[String, String]]) : Seq[HttpHeader] = headers match {
    case Some(headersMap) =>
      headersMap.map(e => RawHeader(e._1, e._2)).toSeq :+ RawHeader("Accept", accept)
    case _ =>
      Seq(RawHeader("Accept", accept))
  }

  override def post[T](url: String, data: T, query: Option[Map[String, String]] = None, headers: Option[Map[String, String]] = None): Future[HttpResponse] = {

    val badSslConfig = AkkaSSLConfig().mapSettings(s => s.withLoose(s.loose.withDisableHostnameVerification(true)
      .withAcceptAnyCertificate(true)))

    val noCertificateCheckContext = ConnectionContext.https(trustfulSslContext, Some(badSslConfig), None, None, None, None)

    val request = HttpRequest(
      method = HttpMethods.POST,
      uri = buildUri(url, query),
      entity = HttpEntity(customContentTypeUrlencoded, data.toString),
      headers = prepareHeaders(headers).to[scala.collection.immutable.Seq]
    )
    Http().singleRequest(request, noCertificateCheckContext)

  }

  def get(url: String, headers: Option[Map[String, String]] = None, parameters: Option[String] = None): Future[HttpResponse] = {
    val badSslConfig = AkkaSSLConfig().mapSettings(s => s.withLoose(s.loose.withDisableHostnameVerification(true)
      .withAcceptAnyCertificate(true)))

    val noCertificateCheckContext = ConnectionContext.https(trustfulSslContext, Some(badSslConfig), None, None, None, None)

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = buildGetUri(url),
      headers = prepareHeaders(headers).to[scala.collection.immutable.Seq]
    )
    Http().singleRequest(request, noCertificateCheckContext)

  }

  private val trustfulSslContext: SSLContext = {

    object NoCheckX509TrustManager extends X509TrustManager {
      override def checkClientTrusted(chain: Array[X509Certificate], authType: String) = ()

      override def checkServerTrusted(chain: Array[X509Certificate], authType: String) = ()

      override def getAcceptedIssuers = Array[X509Certificate]()
    }

    val context = SSLContext.getInstance("TLS")
    context.init(Array[KeyManager](), Array(NoCheckX509TrustManager), null)
    context
  }

}

