package kz.su.unsplash.actors

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.http.scaladsl.model.HttpEntity
import akka.stream.Materializer
import kz.su.domain.{Collection, Photo, SearchCollectionsEntity, SearchPhotosEntity}
import kz.su.serializer.DomainEntity
import kz.su.unsplash.serializers.Serializers
import kz.su.unsplash.util.RestClientImpl
import org.json4s.jackson.JsonMethods._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import akka.util.ByteString

object UnsplashActor {
  case class GetListPhotos(page: Int, pageSize: Int, orderBy: String) extends DomainEntity
  case class GetPhotoById(id: String) extends DomainEntity
  case class GetRandomPhoto() extends DomainEntity
  case class SearchPhotos(query: String, page: Int, pageSize: Int, orientation: String) extends DomainEntity
  case class SearchCollections(query: String, page: Int, pageSize: Int) extends DomainEntity
  case class GetPhotosFromCollection(collectionId: String) extends DomainEntity
  case class GetRelatedCollections(collectionId: String) extends DomainEntity
}

class UnsplashActor(implicit actorSystem: ActorSystem, materializer: Materializer)
  extends Actor
    with ActorLogging
    with Serializers {

  private def unsplashAuth       = new RestClientImpl("https://unsplash.com/oauth/")
  private def unsplashGetRoutes  = new RestClientImpl("https://api.unsplash.com/")

  override def receive: Receive = {
    case UnsplashActor.GetListPhotos(page, pageSize, orderBy) => {
      log.info(s"[LIST OF PHOTO]")
      val response = unsplashGetRoutes.get("photos" + clientId + s"&page=$page&per_page=$pageSize&order_by=$orderBy", headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val photos: Seq[Photo] = parse(body.utf8String).extract[Seq[Photo]]
            context.parent ! photos
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.GetPhotoById(id) => {
      log.info(s"[BY ID]")
      val response = unsplashGetRoutes.get(s"photos/$id" + clientId, headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val photo: Photo = parse(body.utf8String).extract[Photo]
            context.parent ! photo
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.GetRandomPhoto() => {
      log.info(s"[RANDOM]")
      val response = unsplashGetRoutes.get("photos/random" + clientId, headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val photo: Photo = parse(body.utf8String).extract[Photo]
            context.parent ! photo
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.SearchPhotos(query, page, pageSize, orientation) => {
      log.info(s"[SEARCH PHOTOS]")
      val queryParam = if(query.nonEmpty) s"&query=$query"
      val orientationParam = if(orientation.nonEmpty) s"&orientation=$orientation"
      val response = unsplashGetRoutes.get("search/photos"
        + clientId
        + queryParam
        + orientationParam
        + s"&page=$page&per_page=$pageSize")

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val photos: SearchPhotosEntity = parse(body.utf8String).extract[SearchPhotosEntity]
            context.parent ! photos
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.SearchCollections(query, page, pageSize) => {
      log.info(s"[SEARCH COLLECTIONS]")
      val queryParam = if(query.nonEmpty) s"&query=$query"
      val response = unsplashGetRoutes.get("search/collections"
        + clientId
        + queryParam
        + s"&page=$page&per_page=$pageSize", headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val collections: SearchCollectionsEntity = parse(body.utf8String).extract[SearchCollectionsEntity]
            context.parent ! collections
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.GetPhotosFromCollection(collectionId) => {
      log.info(s"[GET PHOTO FROM COLLECTION]")
      val response = unsplashGetRoutes.get(s"collections/$collectionId/photos" + clientId, headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val photos: Seq[Photo] = parse(body.utf8String).extract[Seq[Photo]]
            context.parent ! photos
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

    case UnsplashActor.GetRelatedCollections(collectionId) => {
      log.info(s"[GET RELATED COLLECTIONS]")
      val response = unsplashGetRoutes.get(s"collections/$collectionId/related" + clientId, headers = Some(headers))

      response.onComplete {
        case Success(value) => {
          val entity = value.entity.asInstanceOf[HttpEntity.Default]
          entity.data.runFold(ByteString(""))(_ ++ _).foreach { body =>
            log.info(s"[ENTITY] ${body.utf8String}")
            val collections: Seq[Collection] = parse(body.utf8String).extract[Seq[Collection]]
            context.parent ! collections
          }
        }

        case Failure(exception) =>
          log.info(s"[EXCEPTION] $exception")
          context.parent ! exception
      }
    }

  }

  private val clientId = "?client_id=c1b07e5422ca27e95cad1e582a8fdf9fd85881137a6d518bdc2d48549f583f1a"
  private val headers = Map("Accept-Version" -> "v1")
  private def authRequest = Map(
    "client_id" -> "c1b07e5422ca27e95cad1e582a8fdf9fd85881137a6d518bdc2d48549f583f1a",
    "client_secret" -> "f7ca76b22958d73e06969a36992cf449cb9e753a7ff624f74cc0715700da11eb",
    "redirect_uri" -> "urn:ietf:wg:oauth:2.0:oob",
    "code" -> "1",
    "grant_type" -> "authorization_code"
  )

}
