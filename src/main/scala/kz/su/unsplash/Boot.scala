package kz.su.unsplash

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.util.Timeout
import kz.su.unsplash.actors.UnsplashActor
import kz.su.unsplash.api.RestRoutes

import scala.concurrent.duration._

object Boot extends App with RestRoutes {

  implicit val system = ActorSystem("unsplash-api")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val timeout: Timeout = Timeout(60.seconds)

  def unsplashActor = Props(new UnsplashActor)

  lazy val routes: Route = unsplashRoutes

  Http().bindAndHandle(routes, "0.0.0.0", 6060)

}
